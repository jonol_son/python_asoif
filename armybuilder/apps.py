from django.apps import AppConfig


class ArmyBuilderConfig(AppConfig):
    name = 'armybuilder'
