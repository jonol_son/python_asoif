import json
from enum import Enum

morale_pass_probabilities = {
    2: (36-0) / 36,
    3: (36-1) / 36,
    4: (36-3) / 36,
    5: (36-6) / 36,
    6: (36-10) / 36,
    7: (36-15) / 36,
    8: (36-21) / 36,
    9: (36-26) / 36,
    10: (36-30) / 36,
    11: (36-33) / 36,
    12: (36-35) / 36,
}

UNIT_TYPES = [
    "infantry",
    "monster",
    "cavalry",
    "non-combat unit"
]

FACTIONS = ["lannister", "stark", "neutral"]

CHARISMATIC_FACTION = "neutral"

ATTACK_TYPES = [
    "melee",
    "long range",
    "short range",
]

COSTS = [30, 40, 50]


class Obj:
    pass


def getFactionList(selectedFaction):
    factionList = []
    for faction in FACTIONS:
        factionObj = Obj()
        factionObj.value = faction
        factionObj.isSelected = "selected" if selectedFaction == faction else ""
        factionList.append(factionObj)
    return factionList


def getCostList(selectedCost):
    costList = []
    for cost in COSTS:
        costObj = Obj()
        costObj.value = cost
        costObj.isSelected = "selected" if selectedCost == str(cost) else ""
        costList.append(costObj)
    return costList


def getTypeList(selectedType):
    typeList = []
    for type in UNIT_TYPES:
        typeObj = Obj()
        typeObj.value = type
        typeObj.isSelected = "selected" if selectedType == type else ""
        typeList.append(typeObj)
    return typeList
