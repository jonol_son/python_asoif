from armybuilder.models import ArmyList, Unit, Ability
from rest_framework import serializers


# python manage.py shell
# print(repr(serializers.UnitSerializer()))

class UnitSerializer(serializers.ModelSerializer):
    # id = IntegerField(read_only=True)
    # name = CharField(max_length=200, required=False)
    # title = CharField(max_length=200, required=False)
    # cost = IntegerField(max_value=2147483647, min_value=-2147483648, required=False)
    # type = SerializerMethodField()
    # faction = SerializerMethodField()
    # attachment = BooleanField(required=False)
    # commander = BooleanField(required=False)
    # unique = BooleanField(required=False)

    class Meta:
        model = Unit
        fields = ('id', 'name', 'title', 'cost', 'type', 'faction', 'attachment', 'commander', 'unique')


class UnitPrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    def display_value(self, instance):
        return '%s' % (instance.name)


class CommanderPrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    def display_value(self, instance):
        return '%s' % (instance.name)


class AbilitySerializer(serializers.ModelSerializer):
    # id = IntegerField(read_only=True)
    # name = CharField(max_length=200, required=False)
    # description = CharField(max_length=200, required=False)
    # attacktype = SerializerMethodField()
    # hit = IntegerField(max_value=2147483647, min_value=-2147483648, required=False)
    # high = IntegerField(max_value=2147483647, min_value=-2147483648, required=False)
    # med = IntegerField(max_value=2147483647, min_value=-2147483648, required=False)
    # low = IntegerField(max_value=2147483647, min_value=-2147483648, required=False)
    units = UnitPrimaryKeyRelatedField(allow_empty=False, many=True, queryset=Unit.objects.all())

    class Meta:
        model = Ability
        fields = ('id', 'name', 'description', 'attacktype', 'hit', 'high', 'med', 'low', 'units')


class ArmyListSerializer(serializers.ModelSerializer):
    # id = IntegerField(read_only=True)
    # name = CharField(max_length=200, required=False)
    # cost = IntegerField(max_value=2147483647, min_value=-2147483648, required=False)
    # faction = ChoiceField(choices=(('lannister', 'lannister'), ('stark', 'stark'), ('neutral', 'neutral')), required=False)
    list_commander = CommanderPrimaryKeyRelatedField(queryset=Unit.objects.filter(commander=True))
    units = UnitPrimaryKeyRelatedField(allow_empty=False, many=True, queryset=Unit.objects.all())

    class Meta:
        model = ArmyList
        fields = ('id', 'name', 'cost', 'faction', 'list_commander', 'units')

        list_commander = serializers.SerializerMethodField()

    # def save(self):
    #     units = self.validated_data['units']
