from .serializers import UnitSerializer, AbilitySerializer, ArmyListSerializer
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import viewsets
from armybuilder.models import ArmyList, Unit, Ability, ListUnit
from .filters import UnitFilter, AbilityFilter, ArmyListFilter
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.http import JsonResponse
import json
from rest_framework import status
from rest_framework.response import Response
from .services import ListUnitService

class UnitViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Unit.objects.all()
    serializer_class = UnitSerializer
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    filterset_class = UnitFilter

    @action(methods=['get'], detail=False)
    def newest(self, request):
        newest = self.get_queryset().order_by('id').last()
        serializer = self.get_serializer_class()(newest)
        return Response(serializer.data)

class AbilityViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Ability.objects.all()
    serializer_class = AbilitySerializer
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    filterset_class = AbilityFilter

    @action(methods=['get'], detail=False)
    def newest(self, request):
        newest = self.get_queryset().order_by('id').last()
        serializer = self.get_serializer_class()(newest)
        return Response(serializer.data)

class ArmyListViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = ArmyList.objects.all()
    serializer_class = ArmyListSerializer
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    filterset_class = ArmyListFilter

    @action(methods=['get'], detail=False)
    def newest(self, request):
        newest = self.get_queryset().order_by('id').last()
        serializer = self.get_serializer_class()(newest)
        return Response(serializer.data)

    def patch(self, request):
        content = json.loads(request.body)

        if content['action']== 'addUnit':
            list = ArmyList.objects.get(id=content['id'])
            addedUnit = Unit.objects.get(id=content['unit'])
            ListUnit.objects.create(unit=addedUnit, armylist=list, attachment=None)
            return Response()
        elif content['action']== 'attachAttachment':
            print(content)
            toAttachListUnit = ListUnit.objects.get(id=content['unitListItemId'])
            attachmentListUnit = ListUnit.objects.get(id=content['attachmentid'])
            attachmentUnit = Unit.objects.get(id=attachmentListUnit.unit_id)
            toAttachListUnit.attachment = attachmentUnit
            toAttachListUnit.save()
            attachmentListUnit.delete()
            return Response()

        elif content['action'] == 'removeAttachment':
            ListUnitService.removeAttachment(content['unitListItemId'], ArmyList.objects.get(id=content['id']))
            return Response()
        elif content['action'] == 'removeUnit':
            ListUnitService.removeAttachment(content['unitListItemId'], ArmyList.objects.get(id=content['id']))
            ListUnitService.removeUnit(content['unitListItemId'], ArmyList.objects.get(id=content['id']))
            return Response()
        else:
            return Response(
                {'detail': 'Not a valid action'},
                status=status.HTTP_400_BAD_REQUEST
            )


    def delete(self, request):
        content = json.loads(request.body)
        list = ArmyList.objects.get(id=content['id'])
        list.delete()
        return Response(
            {'detail': 'Not a valid action'},

            status=status.HTTP_200_OK
            # status=status.HTTP_400_BAD_REQUEST
        )
