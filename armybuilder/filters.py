from django_filters import rest_framework as filters
from armybuilder.models import ArmyList, Unit, Ability

class UnitFilter(filters.FilterSet):
    class Meta:
        model = Unit
        fields = {
            'name': ['icontains'],
            'cost': ['iexact'],
            'id': ['iexact', 'gte', 'lte']
        }


class AbilityFilter(filters.FilterSet):
    class Meta:
        model = Ability
        fields = {
            'name': ['icontains'],
            'hit': ['iexact', 'gte', 'lte'],
            'attacktype': ['icontains']
        }

class ArmyListFilter(filters.FilterSet):
    class Meta:
        model = ArmyList
        fields = {
            'name': ['icontains'],
            'cost': ['iexact', 'gte', 'lte']
        }

