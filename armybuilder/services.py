from .enums.enums import getFactionList, getCostList, getTypeList, CHARISMATIC_FACTION, morale_pass_probabilities, \
    FACTIONS, ATTACK_TYPES, UNIT_TYPES
from armybuilder.models import ArmyList, Unit, Ability, ListUnit
from rest_framework import status
from rest_framework.response import Response


def attachDisplayName(units):
    for unit in units:
        unit.display_name = unit.name + ("*" if unit.unique else "") + (", " + unit.title if unit.title else "")
    return units


def set_if_not_none(mapping, key, value):
    if value is not None:
        mapping[key] = value


def addUnitToList(unit, list):
    if unit.type == "Non-Combat Unit":
        list.ncuUnits.append(unit)
    elif unit.attachment == True:
        if unit.type not in list.attachment_dict:
            list.attachment_dict[unit.type] = []
        list.attachment_dict[unit.type].append(unit)
        list.attachments.append(unit)

    else:
        list.selectedUnits.append(unit)
    return list


def calculateCost(allListUnits):
    spentCost = 0
    for unitlistobject in allListUnits:
        unit = Unit.objects.get(id=unitlistobject.unit.id)
        spentCost = unit.cost + spentCost if unit.cost else spentCost
        spentCost = unitlistobject.attachment.cost + spentCost if unitlistobject.attachment and unitlistobject.attachment.cost else spentCost
    return spentCost


def structureList(allListUnits, unitList):
    allUnits = []
    unitList.uniques = []
    for unitlistobject in allListUnits:
        unit = Unit.objects.get(id=unitlistobject.unit.id)
        if unit.unique:
            unitList.uniques.append(unit.name)
        unit.attachedAttachment = unitlistobject.attachment
        if unit.attachedAttachment and unit.attachedAttachment.unique:
            unitList.uniques.append(unit.attachedAttachment.name)
        unit.uniqueId = unitlistobject.id

        allUnits.append(unit)
    allUnits = attachDisplayName(allUnits)

    unitList.ncuUnits = []
    unitList.selectedUnits = []
    unitList.attachment_dict = {}
    unitList.attachments = []

    for unit in allUnits:
        unitList = addUnitToList(unit, unitList)

    # attach potential attachments
    for unit in unitList.selectedUnits:
        unit.potentialAttachments = None
        if not unit.attachedAttachment and unit.type in unitList.attachment_dict:
            unit.potentialAttachments = unitList.attachment_dict[unit.type]

    unitList.spentCost = calculateCost(allListUnits)
    return unitList


def structureLists(lists, request):
    for list in lists:
        if list.owner != request.user:
            list.showTooltip = 'data-toggle=tooltip data-original-title=' + list.owner.username

    return lists;


def updateUnitStats(units):
    for unit in units:
        if unit.defense:
            defense_percent = 100 * ((6 - unit.defense + 1) * (1 / 6))
            unit.defense_percent = "{:.0f}%".format(defense_percent)
            morale_percent = 100 * morale_pass_probabilities[unit.morale]
            unit.morale_percent = "{:.0f}%".format(morale_percent)

        for ability in unit.abilities:
            if ability.hit:
                hit_percent = (6 - ability.hit + 1) * (1 / 6)
                ability.hit_percent = "{:.0f}%".format(100 * hit_percent)
                high_avg_hits = ability.high * hit_percent if ability.high else 0
                ability.high_avg_hits = "~{:.1f}".format(high_avg_hits)
                med_avg_hits = ability.med * hit_percent if ability.med else 0
                ability.med_avg_hits = "~{:.1f}".format(med_avg_hits)
                low_avg_hits = ability.low * hit_percent if ability.low else 0
                ability.low_avg_hits = "~{:.1f}".format(low_avg_hits)
    return units


class ArmyListService():
    def getArmyList(selectedList):
        unitList = ArmyList.objects.get(id=selectedList)
        allListUnits = ListUnit.objects.filter(armylist=unitList)
        unitList = structureList(allListUnits, unitList)

        return unitList

    def getArmyLists(request):
        list_params = {}
        set_if_not_none(list_params, 'cost', request.GET.get('cost'))
        set_if_not_none(list_params, 'faction', request.GET.get('faction'))
        if request.GET.get('showMyLists') == "true" and request.GET.get('showOtherLists') != "true":
            set_if_not_none(list_params, 'owner', request.user)

        lists = ArmyList.objects.filter(**list_params)
        if request.GET.get('showMyLists') != "true" and request.GET.get('showOtherLists') == "true":
            lists = lists.exclude(owner=request.user)

        for unitList in lists:
            allListUnits = ListUnit.objects.filter(armylist=unitList)
            unitList.spentCost = calculateCost(allListUnits)

        lists = structureLists(lists, request)
        return lists


class UnitService():
    def getHirableUnits(unitList):
        factionList = [unitList.faction] if unitList.faction is CHARISMATIC_FACTION else [unitList.faction,
                                                                                          CHARISMATIC_FACTION]
        hirableUnits = Unit.objects.filter(faction__in=factionList, commander=False).exclude(name__in=unitList.uniques)
        hirableUnits = attachDisplayName(hirableUnits)
        return hirableUnits

    def getUnits(selectedFaction, selectedType, showAttachments, showCommanders, showNonCombatAbilities):
        unit_params = {}

        set_if_not_none(unit_params, 'type', selectedType)
        if 'type' not in unit_params:
            set_if_not_none(unit_params, 'type__in', UNIT_TYPES[:-1])
        set_if_not_none(unit_params, 'faction', selectedFaction)
        set_if_not_none(unit_params, 'attachment', showAttachments)
        set_if_not_none(unit_params, 'commander', showCommanders)

        units = Unit.objects.filter(**unit_params)

        for unit in units:
            ability_params = {}

            set_if_not_none(ability_params, 'units', unit)
            set_if_not_none(ability_params, 'attacktype__in', ATTACK_TYPES if not showNonCombatAbilities else None)

            unit.abilities = Ability.objects.filter(**ability_params).order_by('-hit')
            unit.display_name = unit.name + ("*" if unit.unique else "") + (", " + unit.title if unit.title else "")

        units = updateUnitStats(units)

        return units

    def getCommandersForFactions():
        commanders = {}
        for faction in FACTIONS:
            factionList = [faction] if faction is CHARISMATIC_FACTION else [faction, CHARISMATIC_FACTION]
            units = Unit.objects.filter(faction__in=factionList, commander=True)
            commanders[faction] = ",".join([str(unit.id) + "-" + unit.name for unit in units])

        return commanders


class ListUnitService():
    def removeAttachment(uniqueId, list):
        toRemoveListUnit = ListUnit.objects.get(id=uniqueId)
        print("d")
        if (not toRemoveListUnit.attachment_id):
            return Response(
                {'detail': 'No attachment found'},
                status=status.HTTP_400_BAD_REQUEST
            )
        ListUnit.objects.create(unit_id=toRemoveListUnit.attachment_id, armylist=list, attachment=None)

        toRemoveListUnit.attachment = None
        toRemoveListUnit.save()

    def removeUnit(uniqueId, list):
        toRemoveListUnit = ListUnit.objects.get(id=uniqueId)
        toRemoveListUnit.delete()
