from django import forms
from django.forms import ModelChoiceField
from armybuilder.models import ArmyList, Unit, Ability


class CommanderChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return "%s" % (obj.name)


class ArmyListForm(forms.ModelForm):
    class Meta:
        model = ArmyList
        fields = ['name', 'cost', 'faction', 'list_commander']

    def __init__(self, *args, **kwargs):
        super(ArmyListForm, self).__init__(*args, **kwargs)
        self.fields['list_commander'] = CommanderChoiceField(queryset=Unit.objects.filter(commander=True))
        # self.fields['faction'] = ModelChoiceField(queryset=)

        # self.fields['faction'].empty_label = None

    def clean(self):
        cleaned_data = super(ArmyListForm, self).clean()
        list_commander = cleaned_data.get("list_commander")
        faction = cleaned_data.get("faction")
        name = cleaned_data.get("name")
        existingList = None
        try:
            existingList = ArmyList.objects.get(name=name)
        except ArmyList.DoesNotExist:
            pass

        if faction.lower() != 'neutral' and list_commander.faction.lower() != 'neutral' and list_commander.faction.lower() != faction.lower():
            raise forms.ValidationError("Cannot Choose " + faction + " with " + list_commander.faction + " commander")

        if existingList:
            raise forms.ValidationError("A List with this name has already been created " + name )

        return cleaned_data
