from django.db import models
from armybuilder.enums.enums import FACTIONS, UNIT_TYPES, ATTACK_TYPES
from django.contrib.auth.models import User


class CombatUnitManager(models.Manager):
    use_for_related_fields = True

    def cu(self, **kwargs):
        return self.filter('type__in', UNIT_TYPES[:-1], **kwargs)


class Unit(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200, unique=False,
                            default=None)
    title = models.CharField(max_length=200, unique=False,
                             default=None, null=True)
    cost = models.IntegerField(default=0, null=True)
    type = models.CharField(
        max_length=20,
        choices=tuple((unittypes, unittypes)
                      for unittypes in UNIT_TYPES),
        default=None,
    )
    faction = models.CharField(
        max_length=20,
        choices=tuple((faction, faction)
                      for faction in FACTIONS),
        default=None
    )
    speed = models.IntegerField(default=0, null=True)
    defense = models.IntegerField(default=0, null=True)
    morale = models.IntegerField(default=0, null=True)
    attachment = models.BooleanField(default=False)
    commander = models.BooleanField(default=False)
    unique = models.BooleanField(default=False)
    objects = models.Manager()
    cu = CombatUnitManager()


class ArmyList(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200, unique=False,
                            default=None)
    cost = models.IntegerField(default=0)

    faction = models.CharField(
        max_length=20,
        choices=tuple((faction, faction)
                      for faction in FACTIONS),
        default=None
    )
    list_commander = models.ForeignKey(Unit, on_delete=models.CASCADE, related_name='+')
    units = models.ManyToManyField(Unit, through='ListUnit', through_fields=('armylist', 'unit'))
    owner = models.ForeignKey(User, on_delete=models.PROTECT, null=True, default=None)


class ListUnit(models.Model):
    id = models.AutoField(primary_key=True)
    armylist = models.ForeignKey(ArmyList, on_delete=models.CASCADE)
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE)
    attachment = models.ForeignKey(Unit, on_delete=models.CASCADE, related_name='+', null=True)


class Ability(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200, unique=False,
                            default=None)
    description = models.CharField(max_length=200, unique=False,
                                   default=None)
    attacktype = models.CharField(
        max_length=20,
        choices=tuple((attacktype, attacktype)
                      for attacktype in ATTACK_TYPES),
        default=None, null=True
    )
    hit = models.IntegerField(default=0, null=True)
    high = models.IntegerField(default=0, null=True)
    med = models.IntegerField(default=0, null=True)
    low = models.IntegerField(default=0, null=True)
    units = models.ManyToManyField(Unit)


class Resource(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200, unique=False,
                            default=None)
    description = models.CharField(max_length=200, unique=False,
                                   default=None)
    url = models.CharField(
        max_length=50, unique=True,
        default=None, null=True
    )
    image = models.CharField(
        max_length=50, unique=True,
        default=None, null=True
    )
