from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic
from .enums.enums import getFactionList, getCostList, getTypeList, CHARISMATIC_FACTION
from armybuilder import forms
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from armybuilder.models import ArmyList, Unit, Ability, ListUnit, Resource
from armybuilder.services import UnitService, ArmyListService


class SignUp(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'


class About(TemplateView):

    def get(self, request, **kwargs):
        return render(request, 'about.html')

class ViewLists(TemplateView):

    def get(self, request, **kwargs):
        lists = ArmyListService.getArmyLists(request)
        showMyListsChecked = 'checked' if request.GET.get('showMyLists') == 'true' else ''
        showOtherListsChecked = 'checked' if request.GET.get('showOtherLists') == 'true' else ''
        context = {
            'Factions': getFactionList(request.GET.get('faction')),
            'Costs': getCostList(request.GET.get('cost')),
            'Lists': lists,
            'showMyListsChecked': showMyListsChecked,
            'showOtherListsChecked': showOtherListsChecked,
        }

        return render(request, 'viewlists.html', context)


class CreateList(TemplateView):

    def get(self, request, **kwargs):
        username = None
        if request.user.is_authenticated:
            username = request.user.username

        validCommanders = UnitService.getCommandersForFactions()

        context = {
            'form': forms.ArmyListForm(),
            'validCommanders': validCommanders
        }

        for faction in validCommanders:
            context[faction] = validCommanders[faction]

        return render(request, 'createlist.html', context)

    def post(self, request, *args, **kwargs):
        form = forms.ArmyListForm(
            request.POST, initial=request.session.get('form_data'))

        if form.is_valid() and request.user.is_authenticated:
            list = form.save()
            list.owner_id = request.user
            list.save()
            print(list.list_commander.id)
            commanderUnit = Unit.objects.get(id=list.list_commander.id)
            ListUnit.objects.create(unit=commanderUnit, armylist=list, attachment=None)

            return redirect('../editlist/?id=' + str(list.id))

        return render(request, 'createlist.html', {'form': form})


class Browse(TemplateView):
    def get(self, request, **kwargs):
        selectedFaction = request.GET.get('faction')
        selectedType = request.GET.get('type')
        showAttachments = True if request.GET.get('showAttachments') == 'true' else False
        showCommanders = True if request.GET.get('showCommanders') == 'true' else False
        showNonCombatAbilities = True if request.GET.get('showNonCombatAbilities') == 'true' else False

        factionList = getFactionList(selectedFaction)
        typeList = getTypeList(selectedType)

        units = UnitService.getUnits(selectedFaction, selectedType, showAttachments, showCommanders,
                                     showNonCombatAbilities)

        showCommandersChecked = 'checked' if showCommanders else ''
        showAttachmentsChecked = 'checked' if showAttachments else ''
        showNonCombatAbilitiesChecked = 'checked' if showNonCombatAbilities else ''

        context = {'UnitList': units,
                   'Factions': factionList,
                   'Types': typeList,
                   'showAttachmentsChecked': showAttachmentsChecked,
                   'showCommandersChecked': showCommandersChecked,
                   'showNonCombatAbilitiesChecked': showNonCombatAbilitiesChecked}

        return render(request, 'browse.html', context)


class EditList(TemplateView):
    def get(self, request, **kwargs):
        unitList = ArmyListService.getArmyList(request.GET.get('id'))

        username = None
        if request.user.is_authenticated:
            username = request.user.username
        print(username + " " + unitList.owner.username)
        if (unitList.owner.username != username):
            return redirect('../viewlist/?id=' + str(unitList.id))

        hirableUnitList = UnitService.getHirableUnits(unitList)

        context = {
            'unitList': unitList,
            'availableUnitList': hirableUnitList,
        }

        return render(request, 'editlist.html', context)

class Resources(TemplateView):
    def get(self, request, **kwargs):

        resources = Resource.objects.all()
        context = {
            'resources': resources,
        }
        return render(request, 'resources.html', context)

class ViewList(TemplateView):
    def get(self, request, **kwargs):
        unitList = ArmyListService.getArmyList(request.GET.get('id'))
        hirableUnitList = UnitService.getHirableUnits(unitList)

        context = {
            'unitList': unitList,
            'availableUnitList': hirableUnitList,
        }
        return render(request, 'viewlist.html', context)
