"""asoif URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from armybuilder import views, api
from rest_framework import routers
from django.conf.urls import url, include
from django.views.generic.base import TemplateView
from rest_framework.authtoken import views as rest_framework_views
from django.contrib.auth.decorators import login_required

router = routers.DefaultRouter()
router.register(r'units', api.UnitViewSet)
router.register(r'abilities', api.AbilityViewSet)
router.register(r'list', api.ArmyListViewSet)

urlpatterns = [
    path('signup/', views.SignUp.as_view(), name='signup'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', views.Browse.as_view(), name='home'),
    # url(r'^get_auth_token/$', rest_framework_views.obtain_auth_token, name='get_auth_token'),
    url(r'^api/', include(router.urls)),
    # url(r'about/', views.About.as_view(), name='about'),
    url(r'browse/', views.Browse.as_view(), name='browse'),
    url(r'createlist/', login_required(views.CreateList.as_view()), name='createlist'),
    url(r'viewlists/', views.ViewLists.as_view(), name='viewlists'),
    url(r'editlist/', views.EditList.as_view(), name='editlist'),
    url(r'viewlist/', views.ViewList.as_view(), name='viewlist'),
    url(r'resources/', views.Resources.as_view(), name='resources'),
]
